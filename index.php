<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Exercise</title>
    <link rel="stylesheet" href="./assests/css/bootstrap.min.css">
    <link rel="stylesheet" href="style.css">

</head>


<body>
    <div class="container">
        <div class="row">
            <div class="col-md-6 mt-5">
                <img src="./images/building2.jpg" class="img1" style="width: 400px; height: 500px;">
                <img src="./images/building1.webp" class="img2" style="width: 400px; height: 500px;">
                <img src="./images/building.jpg" class="img3" style="width: 400px; height: 500px;">
            </div>
            <div class="col-md-6 mt-5">
                <h6 class="text-danger">About Us</h6>
                <h4 class="text-dark mt-3">Luxury Homes & Flat around <br> the world</h4>
                <p class="text-dark mt-4"> Home is a safe haven and a comfort zone. A place to live with our families and pets and enjoy with friends.</p>
                <br>
                <p class="text-dark">The living room is the most beautiful part of the house. It has a comfortable sofa set, a television, and a bookshelf. The walls are painted in a light color, which makes the room look bright and spacious. The kitchen is well-equipped with all the modern appliances and utensils.</p>
                <div class="row">
                    <div class="col-md-4 col-sm-12 col-xsm-12 col-lg-6 col-xl-4">
                        <div class="card " style=" background-color: white;">
                            <div class="card-body text-center ">
                                <img src="./images/614405.webp" style="width: 50px; height:50px; padding: 10px; background-color: orangered; " class="rounded-circle mt-4">
                                <p class="mt-4">Apartment</p>

                            </div>
                        </div>
                    </div>
                    <div class=" col-md-4  col-sm-12 col-xsm-12 col-lg-6 col-xl-4 ">
                        <div class="card " style=" background-color: white;">
                            <div class="card-body text-center ">
                                <img src="./images/4565530.png" style="width: 50px; height:50px; padding: 10px; background-color: orangered; " class="rounded-circle mt-4">
                                <p class="mt-4">House</p>
                            </div>
                        </div>
                    </div>
                    <div class=" col-md-4  col-sm-12 col-xsm-12 col-lg-6 col-xl-4">
                        <div class="card" style=" background-color: white;">
                            <div class="card-body text-center ">
                                <img src="./images/1973061.webp" style="width: 50px; height:50px; padding: 10px; background-color: orangered; " class="rounded-circle mt-4">
                                <p class="mt-4">Commercial</p>
                            </div>
                        </div>
                    </div>
                </div>
                <button class="button">Contact Us</button>

            </div>
        </div>
    </div>




    <script src="./assests/js/bootstrap.min.js"></script>
</body>

</html>